<?php
$pageTitle = "Dashboard - Clients List";
include_once "partials/header.php";

$service_id = $_GET['service'];
$clients = Query::select('clients', $service_id);
$services = Query::select('categories');
?>


<section id="body">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php include_once "partials/message.php"; ?>
                <div class="panel panel-default">
                    <div class="panel-heading">

                        <div class="btn-group">
                            <button type="button" class="btn btn-primary">Add Client</button>
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu">
                                <?php foreach ($services as $service) { ?>
                                    <li>
                                        <a href="add-client.php?service=<?php echo $service['id']; ?>"><?php echo $service['name'] ?></a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>

                        <div class="btn-group pull-right">
                            <button type="button" class="btn btn-info">Filter by</button>
                            <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="index.php">Full list of clients</a></li>
                                <?php foreach ($services as $service) { ?>
                                    <li class="<?php echo $service['id'] == $service_id ? 'active' : ''; ?>">
                                        <a href="index.php?service=<?php echo $service['id']; ?>"><?php echo $service['name'] ?></a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                    <div class="panel-body">
                        <table class="table">
                            <thead>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th width="150">Action</th>
                            </thead>
                            <tbody>

                            <?php if (!empty($clients->num_rows > 0)): ?>

                                <?php foreach ($clients as $row): ?>

                                    <tr>
                                        <td><?php echo $row["id"]; ?></td>
                                        <td><?php echo $row["name"]; ?></td>
                                        <td><?php echo $row["email"]; ?></td>
                                        <td><?php echo $row["phone"]; ?></td>
                                        <td>
                                            <a href="edit-client.php?id=<?php echo $row["id"]; ?>"
                                               class="btn btn-primary"><span
                                                    class="glyphicon glyphicon-edit"></span></a>

                                            <a onclick="return confirm('Are you sure?')"
                                               href="delete-client.php?id=<?php echo $row["id"]; ?>"
                                               class="btn btn-danger"><span
                                                    class="glyphicon glyphicon-trash"></span></a>

                                            <a href="view-client.php?id=<?php echo $row["id"]; ?>"
                                               class="btn btn-danger"><span
                                                    class="glyphicon glyphicon-eye-open"></span></a>
                                        </td>
                                    </tr>

                                <?php endforeach; ?>

                            <?php else: ?>
                                <tr>
                                    <td colspan="5">
                                        <h3>No item found !</h3>
                                    </td>
                                </tr>
                            <?php endif; ?>

                            </tbody>
                        </table>


                    </div>
                </div>

            </div>
        </div>

    </div>
</section>

<?php include_once "partials/footer.php"; ?>
