<?php if($_GET['status'] == 'success'): ?>
    <div class="alert alert-success">
        <strong>Successfully one row affected!</strong>
        <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
    </div>
<?php elseif ($_GET['status'] == 'error'):?>
    <div class="alert alert-danger">
        <strong>Opps!</strong> Something wrong with you! Please try again.
        <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
    </div>
<?php endif; ?>