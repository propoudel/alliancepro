<?php

include_once 'classes/database.php';
include_once 'classes/query.php';

$action = $_GET['action'];


function method()
{
    $method = $_SERVER['REQUEST_METHOD'];
    return $method;
}


function getList()
{
    $query_result = Query::select();
    $result = array();
    while ($row = mysqli_fetch_assoc($query_result)) {
        $result[] = array(
            'id' => $row["id"],
            'name' => $row["name"],
            'email' => $row["email"],
            'phone' => $row["phone"],
        );

    }

    return $result;
}


function getClient()
{
    $id = $_GET['id'];
    if ((int)$id > 0) {
        $query_result = Query::get_client_by_id($id);
        $result = mysqli_fetch_assoc($query_result);
    } else {
        $result = '';
    }
    return $result;
}


function createClient()
{
    $method = method();
    if ($method == 'POST') {

        $name = $_POST['name'];
        $email = $_POST['email'];
        $phone = $_POST['phone'];
        $query_result = Query::insert_client($name, $email, $phone);
        if ($query_result) {
            return true;
        } else {
            return false;
        }

    } else {
        $status = '400 Bad Request';
        return $status;

    }
}


function updateClient()
{
    $id = $_POST['id'];
    $name = $_POST['name'];
    $email = $_POST['email'];
    $phone = $_POST['phone'];
    $query_result = Query::update_client($name, $email, $phone, $id);
    if ($query_result) {
        return true;
    } else {
        return false;
    }
}

function deleteClient()
{
    $method = method();
    if ($method = "GET") {
        $id = $_GET['id'];
        $query_result = Query::delete_client($id);
        if ($query_result) {
            return true;
        } else {
            return false;
        }

    } else {
        $status = '400 Bad Request';
        return $status;
    }

}


if (isset($action)) {

    switch ($action) {

        case 'get_list':

            $value = getList();

            break;

        case 'get_client':

            $value = getClient();

            break;

        case 'insert_client':

            $value = createClient();

            break;

        case 'update_client':

            $value = updateClient();

            break;

        case 'delete_client':

            $value = deleteClient();

            break;

        default:

            $value = null;

            break;
    }

}

exit(json_encode($value));

?>