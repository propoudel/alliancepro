<?php

/*
 * @Author Tuklal Poudel <developer.sagarpoudel@gmail.com>
 * @Version 1.0
 * @Package Database Query
 */

class Query extends Database
{
    private function __construct()
    {
        parent::__construct();
    }


    public static function run($sql)
    {
        return parent::getInstance()->getConnection()->query($sql);
    }


    public static function select($table, $serviceid = null)
    {
        $sql = "SELECT * FROM $table ORDER BY id DESC";
        if ($serviceid)
            $sql = "SELECT * FROM $table WHERE categoryID = '$serviceid' ORDER BY id DESC";
        return parent::getInstance()->getConnection()->query($sql);
    }


    public static function insert_client($name, $email, $phone, $cat_id)
    {
        $sql = "INSERT INTO `clients` (name, email,phone, categoryID) 
                            VALUES
                              ('$name', '$email', '$phone', $cat_id)";
        return parent::getInstance()->getConnection()->query($sql);
    }


    public static function get_client_by_id($id)
    {
        $sql = "SELECT * FROM clients WHERE id = '$id'";
        return parent::getInstance()->getConnection()->query($sql);
    }


    public static function get_service_by_id($id)
    {
        $sql = "SELECT * FROM categories WHERE id = '$id'";
        return parent::getInstance()->getConnection()->query($sql);
    }


    public static function update_client($name, $emal, $phone, $cat_id, $id)
    {
        $sql = "UPDATE 
                    `clients` 
                  SET
                    `name` = '$name',
                    `email` = '$emal',
                    `phone` = '$phone',
                    `categoryID` = '$cat_id' 
                  WHERE `id` = '$id'";
        return parent::getInstance()->getConnection()->query($sql);
    }


    public static function delete_client($id)
    {
        $sql = "DELETE FROM clients WHERE id = '$id'";
        return parent::getInstance()->getConnection()->query($sql);
    }


}


?>