<?php

include_once 'classes/database.php';
include_once 'classes/query.php';

$id = $_GET['id'];

$query_result = Query::delete_client($id);

if($query_result){
    header('Location: index.php?status=success');
    $closeConnection = $db->__destruct();

}else{
    header('Location: index.php?status=error');
}

?>