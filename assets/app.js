(function ($) {

    //Create client
    $("#form_id").submit(function (event) {
        event.preventDefault();

        var method = $(this).attr("method"),
            action = $(this).attr("action"),
            values = $(this).serialize();

        $.post(action, values, function (response) {
            console.log(response);
        });

    });


}(jQuery));


function sleep(milliseconds) {
    var start = new Date().getTime();
    for (var i = 0; i < 1e7; i++) {
        if ((new Date().getTime() - start) > milliseconds){
            break;
        }
    }
}