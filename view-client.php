<?php
$pageTitle = "Client information";
include('partials/header.php');

$id = $_GET['id'];
$api_url = 'http://alliancepro.dev/api.php?action=get_client&id=' . $id;
$result = mysqli_fetch_assoc(Query::get_client_by_id($id));
$cat_id = $result['categoryID'];
$service = mysqli_fetch_assoc(Query::get_service_by_id($cat_id));
?>


<section id="body">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Details
                    </div>
                    <div class="panel-body">
                        <h3>Client information:</h3>
                        <ul class="list-group" id="client_list">
                            <li class="list-group-item text-center">
                                <strong>Loading...</strong>
                            </li>
                        </ul>
                    </div>
                    <div class="panel-footer">
                        <a href="index.php" class="btn btn-default"><span class="glyphicon glyphicon-menu-left"></span>
                            Back to the list</a>
                        <a onclick="return confirm('Are you sure?')"
                           href="delete-client.php?id=<?php echo $result["id"]; ?>" class="btn btn-danger pull-right">Delete
                            client</a>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>

<?php include('partials/footer.php'); ?>

<script type="text/javascript">
    $(window).bind("load", function () {


        $.get("<?php echo $api_url; ?>", function (data) {

            result = JSON.parse(data);

            html = '<li class="list-group-item"><strong>Name:</strong>' + result.name + '</li>';
            html += '<li class="list-group-item"><strong>Email:</strong> ' + result.email + '</li>';
            html += '<li class="list-group-item"><strong>Phone:</strong> ' + result.phone + '</li>';
            html += '<li class="list-group-item"><strong>Service:</strong><?php echo $service['name'] ?></li>';

            sleep(10000);

            $("#client_list").html(html);

        });


    });

</script>
