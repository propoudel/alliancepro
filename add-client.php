<?php
$pageTitle = "Add new client";
include('partials/header.php');

$db = Database::getInstance();
$mysqli = $db->getConnection();

if(isset($_POST['submit']))
{
    if (!empty($_POST['name']) && !empty($_POST['email']) && !empty($_POST['phone'])) {
        $name = htmlentities($_POST['name']);
        $email = htmlentities($_POST['email']);
        $phone = htmlentities($_POST['phone']);
        $cat_id =  htmlentities($_POST['cat_id']);
        $result = Query::insert_client($name, $email, $phone, $cat_id);
        if($result)
            header('Location: index.php?status=success');
        else
            header('Location: index.php?status=error');
    }
}
?>


    <section id="body">
        <div class="container">
            <div class="row">
                <div class="col-md-12">


                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Add new client
                        </div>
                        <div class="panel-body">
                            <form novalidate action="http://alliancepro.dev/add-client.php" method="post" id="addClientForm">
                                <input type="hidden" name="cat_id" value="<?php echo $_GET['service']; ?>">
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" data-error="Please enter name" class="form-control" id="name"
                                           name="name" placeholder="Name">
                                    <?php if(isset($_POST['name']) and empty($_POST['name'])) : ?>
                                        <small class="has-error">Name field is required!</small>
                                    <?php endif; ?>
                                </div>

                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" data-error="Please enter name" class="form-control" id="email"
                                           name="email" placeholder="Email">
                                    <?php if(isset($_POST['email']) and empty($_POST['email'])) : ?>
                                        <small class="has-error">Email is not valid!</small>
                                    <?php endif; ?>
                                </div>

                                <div class="form-group">
                                    <label for="phone">Phone</label>
                                    <input type="tel" data-error="Please enter name" class="form-control" id="phone"
                                           name="phone" placeholder="Phone">
                                    <?php if(isset($_POST['phone']) and empty($_POST['phone'])) : ?>
                                        <small class="has-error">Phone field is required</small>
                                    <?php endif; ?>

                                </div>

                                <button type="submit" name="submit" class="btn btn-primary btn-lg">Add Client</button>
                                <a href="./index.php" name="cancel" class="btn btn-default btn-lg">Cancel</a>

                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

<?php include('partials/footer.php'); ?>