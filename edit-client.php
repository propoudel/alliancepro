<?php
$pageTitle = "Edit client";
include('partials/header.php');

$db = Database::getInstance();
$mysqli = $db->getConnection();

$id = $_GET['id'];
$query_result = Query::get_client_by_id($id);
$result = mysqli_fetch_assoc($query_result);
$services = Query::select('categories');

$name = $result['name'];
$email = $result['email'];
$phone = $result['phone'];
$cat_id = $result['categoryID'];

if(isset($_POST['submit']))
{
    if (!empty($_POST['name']) && !empty($_POST['email']) && !empty($_POST['phone'])) {
        $name = htmlentities($_POST['name']);
        $email = htmlentities($_POST['email']);
        $phone = htmlentities($_POST['phone']);
        $cat_id =  htmlentities($_POST['cat_id']);
        $result = Query::update_client($name, $email, $phone, $cat_id, $id);
        if($result)
            header('Location: index.php?status=success');
        else
            header('Location: index.php?status=error');
    }
}
?>

    <section id="body">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Edit - <?php echo $name; ?>
                        </div>
                        <div class="panel-body">
                            <form action="http://alliancepro.dev/edit-client.php?id=<?php echo $id; ?>" method="post" id="addClientForm">
                                <input type="hidden" name="cat_id" value="<?php echo $cat_id; ?>">
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" value="<?php echo $name ?>" data-error="Please enter name" class="form-control" id="name"
                                           name="name" placeholder="Name">
                                    <?php if(isset($_POST['name']) and empty($_POST['name'])) : ?>
                                        <small class="has-error">Name field is required</small>
                                    <?php endif; ?>
                                </div>

                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input value="<?php echo $email ?>" type="email" data-error="Please enter name" class="form-control" id="email"
                                           name="email" placeholder="Email">
                                    <?php if(isset($_POST['email']) and empty($_POST['email'])) : ?>
                                        <small class="has-error">Email is not valid!</small>
                                    <?php endif; ?>
                                </div>

                                <div class="form-group">
                                    <label for="phone">Phone</label>
                                    <input value="<?php echo $phone ?>" type="tel" data-error="Please enter name" class="form-control" id="phone"
                                           name="phone" placeholder="Phone">
                                    <?php if(isset($_POST['phone']) and empty($_POST['phone'])) : ?>
                                        <small class="has-error">Phone field is required</small>
                                    <?php endif; ?>

                                </div>

                                <div class="form-group">
                                    <label for="service">Change Service</label>
                                    <select class="form-control" name="cat_id">
                                        <?php while ($row = mysqli_fetch_assoc($services)) { ?>
                                            <option <?php echo $row['id'] == $cat_id ? 'selected' : ''; ?> value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
                                        <?php } ?>
                                    </select>


                                </div>

                                <button type="submit" name="submit" class="btn btn-primary btn-lg">Update Client</button>
                                <a href="./index.php" name="cancel" class="btn btn-default btn-lg">Cancel</a>

                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

<?php include('partials/footer.php'); ?>